{ config, lib, ... }:
with lib;
let
  cfg = config.services.financier;
in
{
  imports = [ ];

  options.services.financier = {
    enable = mkEnableOption "Financier";

    hostName = mkOption {
      type = types.str;
      description = ''
        Name for the virtual host for the web server.
      '';
      example = "example.org";
    };

    openFirewall = mkOption {
      description = ''
        Whether to open ports 80 and 443 in the firewall for the web server that is serving financier.
      '';
      type = types.bool;
      default = true;
    };

    package = mkOption {
      type = types.package;
      description = ''
        Financier package to use.
      '';
    };

    server = mkOption {
      type = types.enum [
        "nginx"
        "caddy"
      ];
      description = ''
        The web server to be used for serving Financier. Either "nginx"
        or "caddy".
      '';
      default = "caddy";
      example = "example.org";
    };
  };

  config = mkIf cfg.enable {
    networking.firewall.allowedTCPPorts = mkIf cfg.openFirewall [
      80
      443
    ];

    services.nginx = mkIf (cfg.server == "nginx") {
      enable = true;

      virtualHosts."${cfg.hostName}" = {
        forceSSL = true;
        enableACME = true;
        root = cfg.package;
        locations."/".tryFiles = "$uri $uri/ /index.html";
      };
    };

    services.caddy = mkIf (cfg.server == "caddy") {
      enable = true;

      extraConfig = ''
        ${cfg.hostName} {
          root * ${cfg.package}
          file_server
          try_files {path} {path}/ /index.html
        }
      '';
    };
  };
}

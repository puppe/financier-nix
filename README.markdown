# Nix support for financier

This repository contains files for building
[financier](https://gitlab.com/mpuppe/financier/) with the
[Nix](https://nixos.org/manual/nix/stable/) package manager. We rely on the
[Flakes](https://nixos.wiki/wiki/Flakes) feature of Nix. Make sure you have
Flakes [enabled](https://nixos.wiki/wiki/Flakes#Installing_flakes) before you
continue.

## Module

There is a module (available as flake output `nixosModule`) that you can use in
your NixOS configuration. See [module.nix](./module.nix) for the available
options. This is intended to be the primary way how to use this flake in your
configurations.

## Building the package

The distribution for financier can be built with the following command:

``` bash
nix build
```

Directory `result` will contain the built distribution which can be served from a web server.

## Questions and Answers

### Why no overlay?

There used to be an
[overlay](https://nixos.org/manual/nixpkgs/stable/#chap-overlays) for the
distribution package. But overlays have the disadvantage (in this case) that
they can be applied to any version of Nixpkgs. But the package, as it is now,
cannot be built with just any version of Nixpkgs. Updates of Nixpkgs in your
configuration can break the package and thus your configuration.

For example, this has happened in May 2022 when Node 12 was marked as insecure
(because it is at the end of its life). But at that point in time, Node 12 was
still used for building our package. The result was that the package build was
broken and with it all configurations that use our module.

Since then, we always use a pinned version of Nixpkgs to build the package
regardless in which configuation the module is used. This has the downside that
we will miss potential security updates. On the other hand, we only build a
static website which is very unlikely to have any serious security issues.

{
  inputs.systems.url = "github:nix-systems/default";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.financier = {
    url = "github:financier-io/financier/19001ceba1404b790d25ed10838728c5c392ef85";
    flake = false;
  };

  outputs = inputs@{ self, nixpkgs, ... }:
    inputs.flake-utils.lib.eachSystem (import inputs.systems)
      (system:
        let pkgs = import nixpkgs { inherit system; };
        in {

          packages = {
            financier-dist = pkgs.mkYarnPackage {
              name = "financier-dist";
              src = "${inputs.financier}";
              buildPhase = ''
                yarn --offline build
              '';
              distPhase = "true";
              installPhase = ''
                cp -r dist "$out/"
              '';
              configurePhase = "ln -s $node_modules node_modules";
            };
          };

          defaultPackage = self.packages.${system}.financier-dist;
        }) // {

      nixosModule = { pkgs, lib, ... }: {
        imports = [ ./module.nix ];
        services.financier.package =
          lib.mkDefault self.packages.${pkgs.system}.financier-dist;
      };

    };
}
